<?php

namespace App\Http\Middleware;

use Closure;

class ApiTokenCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $api_token = $request->get('api_token');

        $pengguna = \App\Pengguna::where('api_token', $api_token)->first();
        if (!$pengguna) {
            return response()->json(['error' => 'Unauthenticated'], 500);
        }
        return $next($request);
    }
}
