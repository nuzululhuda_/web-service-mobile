<?php

namespace App\Http\Controllers;

use Validator;
use App\Kategori;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class KategoriController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kategori = DB::table('kategori')
            ->select('id', 'kategori')
            ->get();
        return response()->json($kategori, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'kategori' => 'required',
        ]);

        if ($validator->fails()){
            return response()->json($validator->messages(), 500);
        }
        
        DB::table('kategori')->insert([
            'kategori' => $request->kategori,
        ]);
        
        return response()->json([
            'success' => true,
            'data' => ['kategori' => $request->kategori],
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $kategori = DB::table('kategori')->where('id', $id)->first();
        return response()->json($kategori, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {       
            $kategori = Kategori::where('id', $id)->firstOrFail();
            $validator = Validator::make($request->all(),[
                'kategori' => 'required',
            ]);

            if ($validator->fails()){
                return response()->json($validator->messages(), 500);
            }
            
            DB::table('kategori')
                ->where('id', $id)
                ->update(['kategori' => $request->kategori]);
            
            return response()->json([
                'success' => true,
                'data' => ['kategori' => $request->kategori],
            ], 200);
    
           
        } catch (ModelNotFoundException $ex) {
            return response()->json([
                'success' => false,
                'message' => 'Data not found'
            ], 404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $kategori = Kategori::where('id', $id)->firstOrFail();
            $kategori->delete();
            return response()->json([
                'success' => true,
            ], 200);
        } catch (ModelNotFoundException $ex) {
            return response()->json([
                'success' => false,
                'message' => 'Data not found'
            ], 404);
        }
        
    }
}
