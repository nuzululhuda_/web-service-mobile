<?php

use Illuminate\Http\Request;
use App\Http\Middleware\ApiTokenCheck;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/', function (Request $request) {
    return response()->json([
        'success' => true,
        'message' => 'Api for mobile practicum' 
    ], 200);
});
Route::prefix('auth')->group(function () {
    Route::post('/register', 'AuthController@register');
    Route::post('/login', 'AuthController@login');
    Route::get('/logout', 'AuthController@logout');
});

Route::middleware(ApiTokenCheck::class)->group(function () {
    Route::get('articles', 'ArtikelController@index');
    Route::post('articles/store', 'ArtikelController@store');
    Route::get('articles/detail/{id}', 'ArtikelController@show');
    Route::delete('articles/delete/{id}', 'ArtikelController@destroy');
    Route::put('articles/update/{id}', 'ArtikelController@update');

    Route::get('categories', 'KategoriController@index');
    Route::post('categories/store', 'KategoriController@store');
    Route::delete('categories/delete/{id}', 'KategoriController@destroy');
    Route::put('categories/update/{id}', 'KategoriController@update');
});

