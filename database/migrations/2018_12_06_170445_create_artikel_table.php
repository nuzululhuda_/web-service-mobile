<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArtikelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('artikel', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('kategori_id');
            
            $table->foreign('user_id')
                ->references('id')->on('pengguna')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            
            $table->foreign('kategori_id')
                ->references('id')->on('kategori')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->text('content');
            $table->string('status');	
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('artikel');
    }
}
